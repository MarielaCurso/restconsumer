Feature: Lista productos
    Scenario: Cargar lista de productos
        When we request the product list
        Then we should recieve
            | nombre | descripcion |
            | Celular XL | Un teléfono con una de las mejores pantallas |
            | Celular Mini | Un télefono mini con una de las mejores cámaras |
            | Celular Standard | Un télefono estándar. Nada especial. |